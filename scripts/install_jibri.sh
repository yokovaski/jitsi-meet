#!/usr/bin/env bash

# Update apt repositories, packages and system
apt-get update
apt-get upgrade -y
apt-get dist-upgrade -y

# Load ALSA loopback module in to the kernel
echo "snd-aloop" >> /etc/modules
modprobe snd-aloop

# Install dependencies
apt-get install -y alsa-utils ffmpeg icewm jq python-pip python3 python3-pip xdotool xserver-xorg-input-void xserver-xorg-video-dummy git curl unzip

# Install Google Chrome
wget https://dl.google.com/linux/linux_signing_key.pub
apt-key add linux_signing_key.pub
echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/dl_google_com_linux_chrome_deb.list
apt-get update
apt-get install -y google-chrome-stable

# Request latest version number of chromedriver
CHD_VER=$(curl -L http://chromedriver.storage.googleapis.com/LATEST_RELEASE)

# Download zipped chromedriver and unzip
wget https://chromedriver.storage.googleapis.com/$CHD_VER/chromedriver_linux64.zip
unzip chromedriver_linux64.zip

# Copy binary
cp chromedriver /usr/bin/chromedriver

# Clean up directory
rm chromedriver
rm chromedriver_linux64.zip
rm linux_signing_key.pub

# Add jibri user and add jibri to correct groups
useradd -ms /bin/bash jibri
usermod -a -G adm,audio,video,plugdev jibri
cd /home/jibri

# Clone Jibri from source
runuser -l jibri -c 'git clone https://github.com/jitsi/jibri.git'
#runuser -l jibri -c 'git clone https://github.com/VoIPGRID/video-meet.git'
#runuser -l jibri -c 'git checkout feature-call-with-pjsua'

# Copy Jibri directories to the Jibri home folder
runuser -l jibri -c 'cp -a jibri/jibri-xmpp-client /home/jibri'
runuser -l jibri -c 'cp -a jibri/scripts /home/jibri'
runuser -l jibri -c 'cp jibri/asoundrc /home/jibri/.asoundrc'

# Copy window manager preferences and connecting image
runuser -l jibri -c 'mkdir /home/jibri/.icewm'
runuser -l jibri -c 'cp jibri/icewm.preferences /home/jibri/.icewm/preferences'
runuser -l jibri -c 'cp jibri/connecting.png /home/jibri/.icewm/connecting.png'

# Copy config
runuser -l jibri -c 'cp jibri/config.json /home/jibri/config.json'

# Install python dependencies
runuser -l jibri -c 'pip3 install --upgrade pip'
runuser -l jibri -c 'pip3 install setuptools --user'
runuser -l jibri -c 'pip3 install -r jibri/requirements.txt --user'

# Copy daemon services
cp jibri/systemd/*.service /etc/systemd/system/

# Enable daemon services
systemctl enable jibri-xorg.service
systemctl enable jibri-icewm.service
systemctl enable jibri-xmpp.service

systemctl start jibri-xorg
systemctl start jibri-icewm