#!/usr/bin/env bash

# Install curl and unzip
apt-get install -y curl unzip

# Request latest version number
CHD_VER=$(curl -L http://chromedriver.storage.googleapis.com/LATEST_RELEASE)

# Download zipped chromedriver and unzip
wget https://chromedriver.storage.googleapis.com/$CHD_VER/chromedriver_linux64.zip
unzip chromedriver_linux64.zip

# Copy binary
cp chromedriver /usr/bin/chromedriver

# Clean up directory
rm chromedriver
rm chromedriver_linux64.zip