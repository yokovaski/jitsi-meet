#!/usr/bin/env bash

echo 'deb https://download.jitsi.org stable/' >> /etc/apt/sources.list.d/jitsi-stable.list
wget -qO -  https://download.jitsi.org/jitsi-key.gpg.key | apt-key add -
apt-get update

apt-get -y install jitsi-meet

sudo /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh

cp images/{favicon.ico,watermark.png} /usr/share/jitsi-meet/images/
cp config/title.html /usr/share/jitsi-meet/

sed -i "/VERTICAL_FILMSTRIP/c\    VERTICAL_FILMSTRIP: false," /usr/share/jitsi-meet/interface_config.js
sed -i "/JITSI_WATERMARK_LINK/c\    JITSI_WATERMARK_LINK: 'https://voipgrid.nl'," /usr/share/jitsi-meet/interface_config.js

sed -i "/disableDesktopSharing/c\    disableDesktopSharing: false," /etc/jitsi/meet/jitsi.voipgrid.nl-config.js
sed -i "/desktopSharingChromeExtId/c\    desktopSharingChromeExtId: 'ionknggehkkcdefildepkmnekagpilcg'," /etc/jitsi/meet/jitsi.voipgrid.nl-config.js
sed -i "/desktopSharingChromeDisabled/c\    desktopSharingChromeDisabled: false," /etc/jitsi/meet/jitsi.voipgrid.nl-config.js