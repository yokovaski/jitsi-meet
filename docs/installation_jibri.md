# Jibri installation
This document describes how Jibri is to be installed on a debian server. 

A working Jitsi-meet [installation](installation_jitsi_meet.md) is required for Jibri to work. Jibri adds streaming 
capabilities to Jitsi-meet.

## 1. Automated installation
Jibri should be installed on a separate server instance. Clone this repo or copy `install_jibri.sh` to the server and 
execute:
```bash
sudo ./install_jibri.sh
```

Now skip part two and continue with part 3 of this guide.

## 2. Install manually
Installation steps for installing Jibri without using `install_jibri.sh`.

### 2.1. ALSA loopback
Make sure the loopback module is available:
```bash
sudo modinfo snd-aloop
```

Now the loopback module should be loaded in to the kernel. Do this with:
```bash
sudo echo "snd-aloop" >> /etc/modules
sudo modprobe snd-aloop
```

Check if the module is loaded by running: `lsmod | grep snd_aloop`.

### 2.2. Install dependencies
Jibri has a variety of dependencies:
  * alsa-utils
  * ffmpeg
  * icewm
  * jq
  * python-pip
  * python3
  * python3-pip
  * xdotool
  * xserver-xorg-input-void
  * xserver-xorg-video-dummy
  
Install these dependencies with:
```bash
sudo apt-get install alsa-utils ffmpeg icewm jq python-pip python3 python3-pip xdotool xserver-xorg-input-void xserver-xorg-video-dummy
```

Add Google Chrome repository and install Google Chrome:
```bash
wget https://dl.google.com/linux/linux_signing_key.pub
sudo apt-key add linux_signing_key.pub
sudo echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/dl_google_com_linux_chrome_deb.list
sudo apt-get update
sudo apt-get install google-chrome-stable
```

Selenium needs Chromedriver to start and control Google Chrome. First request the latest version with:
```bash
curl -L http://chromedriver.storage.googleapis.com/LATEST_RELEASE
```
Then download and install the latest Chromedrive where `$RELEASE_NUMBER` is the result of the previous command:
```bash
wget https://chromedriver.storage.googleapis.com/$RELEASE_NUMBER/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
sudo cp chromedriver /usr/bin/chromedriver
```

### 2.3. Jibri user
Add the jibri user and add this user to the needed groups:
```bash
sudo useradd -ms /bin/bash jibri
sudo usermod -a -G adm,audio,video,plugdev jibri
```

### 2.4 Clone Jibri and set up directory
Change to the jibri user with `sudo su - jibri`. First clone Jibri and the copy the needed directories and files to `/home/jibri`:
```bash
git clone https://github.com/jitsi/jibri.git
cp -a jibri/jibri-xmpp-client /home/jibri 
cp -a jibri/scripts /home/jibri
cp jibri/asoundrc /home/jibri/.asoundrc
mkdir /home/jibri/.icewm
cp jibri/icewm.preferences /home/jibri/.icewm/preferences
cp jibri/connecting.png /home/jibri/.icewm/connecting.png
cp jibri/config.json /home/jibri/config.json
```
### 2.5 Install python dependencies
Install the python dependencies of Jibri with:
```bash
pip3 install --upgrade pip
pip3 install setuptools
pip3 install -r jibri/requirements.txt
```

## 3. Enable use of Jibri in Jitsi Meet
In order to be able to use the live stream capabilities of Jibri in Jitsi Meet, the use of Jibri should be enabled. You 
should logged into the Jitsi Meet server for this part.

### 3.1. Open port 5222
The first step is to allow the Jibri instance to connect to Jitsi. This can be done by opening port 5222 for TCP traffic 
the Jibri instance. 

Login to the Jitsi server and add the following line to `/etc/iptables/rules.v4` where `$IPADDRESS` is the ip address of
the Jibri instance:
```bash
-A INPUT -s $IPADDRESS/32 -p tcp -m state --state NEW -m comment --comment "Jibri instance" -m tcp --dport 5222 -j ACCEPT
```

### 3.2. Prosody
Create the recorder virtual host entry in /etc/prosody/prosody.cfg.lua:
```
VirtualHost "recorder.jitsi.voipgrid.nl"
    modules_enabled = {
        "ping";
    }
    authentication = "internal_plain"
```

Create an account for Jibri and record the password you choose, as it will be used in config.json below:
```bash
prosodyctl register jibri auth.jitsi.voipgrid.nl <jibripassword>
```

Create an account for Selenium and record the password you choose, as it will be used in config.json below:
```bash
prosodyctl register recorder recorder.yourdomain.com <recorderpassword>
```

### 3.3. Jicofo
Add the following lines to `/etc/jitsi/jicofo/sip-communicator.properties`:
```bash
org.jitsi.jicofo.jibri.BREWERY=TheBrewery@conference.yourdomain.com
org.jitsi.jicofo.jibri.PENDING_TIMEOUT=90
```

Now restart Jicofo to enable these settings:
```bash
sudo systemctl restart jicofo
```

### 3.4. Jitsi Meet
Enable jibri in the jitsi-meet config by executing:
```bash
sudo sed -i "/enableRecording/c\    enableRecording: true,\n    hiddenDomain: 'recorder.jitsi.voipgrid.nl'," /etc/jitsi/meet/jitsi.voipgrid.nl-config.js
sudo sed -i "/recordingType/c\    recordingType: 'jibri'," /etc/jitsi/meet/jitsi.voipgrid.nl-config.js
```

Once recording is enabled in the config.js, the recording button will become available in the user interface. However, until 
a valid jibri is seen by Jicofo, the mesage "Recording currently unavailable" will be displayed when it is pressed. Once 
a jibri connects successfully, the user will instead be prompted to enter a stream key.

## 4. Configure Jibri
Log into the Jibri server. Now set the correct domain, users and passwords in `/home/jibri/config.json`. For example:
```json
{
    "jidserver_prefix":"auth.",
    "mucserver_prefix":"conference.",
    "boshdomain_prefix":"",
    "password":"<jibripassword>",
    "recording_directory":"./recordings",
    "jid_username":"jibri",
    "roomname":"TheBrewery",
    "xmpp_domain":"jitsi.voipgrid.nl",
    "selenium_xmpp_prefix":"recorder.",
    "selenium_xmpp_username":"recorder",
    "selenium_xmpp_password":"<recorderpassword>",
    "servers":["jitsi.voipgrid.nl"],
    "environments":{
    }
}
```

Now reboot the Jibri server and wait for it to come back online. Open a new Jitsi Meet URL and see if the recording 
option is enabled and prompts for a stream key. If so: Jibri has succesfully been connected to the Jitsi Meet server!

Not restarting will result in Jibri not being able to make use of audio devices.

## 5. Troubleshooting

### Jibri logging
* Jibri writes to syslog as app.py. The output of Jibri can be followed with:
```bash
tail -f /var/log/syslog | grep -E app.py
```

### Check Selenium
* Open a browser to `jitsi.voipgrid.nl/testing`. Test if Chrome can join the session:
```bash
sudo su - jibri
cd /home/jibri/jibri-xmpp/
DISPLAY=:0 ./jibriselenium.py -u https://jitsi.voipgrid.nl/testing
```

### Check audio recording
* Check the logging in `/tmp/jibri-audio_check.log`.

* If the audio device can not be found, check if the jibri user is in the audio group. If that is the case a restart may 
  fix the issue.

* Check if audio can be recorded with:
```bash
sudo su - jibri
ffmpeg -f alsa -i hw:0,1,0 videoconference.wav
```
* Alternatively check if audio can be recorded with:
```bash
arecord -D hw:0,1,0 -f S16_LE -c 2 -r 44100 test.wav
```

### Time outs
* Time outs of Selenium trying to connect to a video conference can be caused by using ipv6. Disable the ipv6 localhost in 
`/etc/hosts` and try again.

* Currently there is an issue with the latest google-chrome-stable and Chromedriver. Playing a .wav file causes a freeze 
of Jibri during the audio check (see [this](https://github.com/jitsi/jibri/issues/63) issue). Setting the 
`default_audio_url` to another audio playing web page will solve this problem.