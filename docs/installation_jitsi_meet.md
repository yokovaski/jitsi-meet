# Installation Jitsi Meet
This document describes the installation of Jitsi Meet on a Debian server.

## 1. Nginx
Nginx is needed for serving Jitsi-meet-web and Prosody. Install Nginx with:

```bash
sudo apt-get install nginx
```

Open iptables to allow TCP traffic from port 80 and 443 as wel as UDP traffic from port 10000:
```bash
nano /etc/iptables/rules.v4
```

And add these line to the file: 
```bash
-A INPUT -p tcp -m state --state NEW -m comment --comment "http and https from the WWW" -m multiport --dports 80,443 -j ACCEPT
-A INPUT -p udp -m udp --dport 10000 -j ACCEPT
```

Save and close the file. Enable the new iptables settings with:
```bash
iptables-restore -n < /etc/iptables/rules.v4
iptables-save
```

Make sure Nginx can be reached on `jitsi.voipgrid.nl` port 80 and port 443.

## 2. Jitsi-meet
Install jitsi-meet either from script or manually.

### 2.1. Install from script
Clone this repo and run this command:
```bash
sudo ./scripts/install_jitsi_meet.sh
```

Set the hostname to `jitsi.voipgrid.nl`.

Open Google Chrome, Chromium, Firefox or Opera and enter `jitsi.voipgrid.nl`. Confirm that Jitsi-meet is reachable and 
that the Let's Encrypt certificate is installed.

Chrome or Chromium browsers should install [this](../bin/chrome.crx) extension to be able to make use of screen sharing.

### 2.2. Install manually

First add the Jitsi-meet repository and update the package list:

```bash
echo 'deb https://download.jitsi.org stable/' >> /etc/apt/sources.list.d/jitsi-stable.list
wget -qO -  https://download.jitsi.org/jitsi-key.gpg.key | apt-key add -
apt-get update
```

Now install jitsi-meet-web, jicofo, prosody and jitsi-videobridge:
```bash
apt-get -y install jitsi-meet
```

Set the hostname to `jitsi.voipgrid.nl`.

Enable SSL for jitsi-meet-web:
```bash
sudo /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh
```

Open Google Chrome, Chromium, Firefox or Opera and enter `jitsi.voipgrid.nl`. Confirm that Jitsi-meet is reachable and 
that the Let's Encrypt certificate is installed.

Clone this repo and change your directory to this repo. Copy the VoIPGRID logo to the correct directories and enable 
them:
```bash
cp images/{favicon.ico,watermark.png} /usr/share/jitsi-meet/images/
cp config/title.html /usr/share/jitsi-meet/
```

Switch to the thumbnail layout to the horizontal view and set the correct link for the watermark:
```bash
sed -i "/VERTICAL_FILMSTRIP/c\    VERTICAL_FILMSTRIP: false," /usr/share/jitsi-meet/interface_config.js
sed -i "/JITSI_WATERMARK_LINK/c\    JITSI_WATERMARK_LINK: 'https://voipgrid.nl'," /usr/share/jitsi-meet/interface_config.js
```

Enable screen sharing. Chrome or Chromium browsers should install [this](../bin/chrome.crx) extension.
```bash
sed -i "/disableDesktopSharing/c\    disableDesktopSharing: false," /etc/jitsi/meet/jitsi.voipgrid.nl-config.js
sed -i "/desktopSharingChromeExtId/c\    desktopSharingChromeExtId: 'ionknggehkkcdefildepkmnekagpilcg'," /etc/jitsi/meet/jitsi.voipgrid.nl-config.js
sed -i "/desktopSharingChromeDisabled/c\    desktopSharingChromeDisabled: false," /etc/jitsi/meet/jitsi.voipgrid.nl-config.js
```

## 3. Jigasi
Install Jigasi to add VoIP calling to a Jitsi-meet conference. Beware of the risks of this functionality. Every video 
conference user will be able to call every number that is allowed to call.

Install Jigasi:
```bash
apt-get -y install jigasi
```

Fill in the VoIP account credentials. For example `12345678@ha.voipgrid.nl` and `sercretvoippassword`.

## 4. Jibri
[Install](installation_jibri.md) Jibri to add live stream capabilities to jitsi-meet.