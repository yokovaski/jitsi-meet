# Jitsi-meet architecture
Jitsi-meet is a combination of applications of Atlassian. These applications are responsible for delivering video 
calling to its end users.

## Components
Jitsi-meet consists out of the applications Prosody, Jicofo, Jitsi-videobridge and Jitsi-meet-web for delivering basic 
video calling.

Capabilities of Jitsi-meet can be extended with Jibri, Jigasi and Jidesha.

![Jitsi-meet](../images/jitsi_meet_architecture.png)

### Prosody
Prosody is a XMPP communication server. It is responsible for delivering messaging capabilities to the end user. It 
does this by creating chat rooms. When a user joins a non existing room in Prosody, Prosody will create this room.

Jitsi-meet uses Prosody for creating and managing rooms as well as the messaging capabilities. A user that joins a 
Jitsi-meet video conference, initially joins a Prosody room.

### Jicofo
Jicofo is the application that controls Prosody and Jitsi-videobridge. When a user creates a new room in Prosody, 
Jicofo will join that room as administrator. 

When another user joins this created room, Jicofo will notify Jitsi-videobridge. Jitsi-videobridge therefore knows 
where it has to send the videostream of the first user to the second user and vice versa.

### Jitsi-videobridge
Jitsi-videobridge (JVB) is responsible for receiving and sending video streams. JVB does not mix video streams. JVB is 
relatively light on hardware because it does not decode and encode video streams. It does need to run on a server with 
good network bandwidth. 

Every user in a Prosody room receives as much video streams of other users as possible. Video streams will be send 
based on loudness. The loudest video stream will be send in high definition. Other video streams will be send in low 
definition to save bandwidth. Users can override which video stream will be send in high definition.

### Jibri
Jibri can be used for sending video of a video conference to YouTube. A live stream key of YouTube is needed for 
sending the video.

Jibri needs to run on a separate server. Jibri start Google Chrome in a Selenium environment. Jibri will set the url of 
Google Chrome to the url of the video conference that needs to be live streamed. 

Google Chrome will be rendered on a virtual display. Jibri starts FFmpeg to capture this virtual display and uses audio 
loopback devices for capturing the sound.

FFmpeg is capable of sending a video stream directly to YouTube based on a live stream key.

### Jigasi
Jigasi can be used for adding VoIP to Jitsi-meet. Jigasi needs to be configured with a valid VoIP account. Jigasi will 
register itself as aSIP User Agent at a SIP proxy. 

Every video conference uses the same VoIP account for VoIP calling.

### Jidesha
Jidesha is a plugin that is needed for Google Chrome and Chromium variants to enable screen sharing.